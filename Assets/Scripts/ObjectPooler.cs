﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {
	[Header("Префабы для пула:")][SerializeField] 
	GameObject[] _prefabs;
	
	[Header("Число мышей в пуле:")][SerializeField] 
	int _poolSize;
	
	public List<GameObject> _poolObjects;

	void Awake () {
		_poolObjects = new List<GameObject>();
		
		InstantiatePool();
	}

	void InstantiatePool() {
		int currentPrefabIndex = 0;
		
		for (int i = 0; i < _poolSize; i++) {
			
			if (currentPrefabIndex == _prefabs.Length) {
				currentPrefabIndex = 0;
			}

			GameObject obj = Instantiate(_prefabs[currentPrefabIndex], gameObject.transform);
			obj.SetActive(false);
			_poolObjects.Add(obj);
		
			currentPrefabIndex++;
			
		}
	}

	public void ReturnObject(GameObject obj) {
		obj.SetActive(false);
	}

	public GameObject GetObject() {
		
		foreach (var obj in _poolObjects) {
			if (! obj.activeInHierarchy) {
				obj.SetActive(true);
				return obj;
			}
		}

		GameObject newGameObject = Instantiate(_prefabs[Random.Range(0, _prefabs.Length)]);
		_poolObjects.Add(newGameObject);
		return newGameObject;
		
	}
}
