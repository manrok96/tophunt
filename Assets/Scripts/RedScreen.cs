﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class RedScreen : MonoBehaviour {
	[SerializeField] Color _colorRed;
	Color _defaultColor;

	[SerializeField] float _speedReturnDefaultColor = 1.5f;

	Image _panel;

	void Start() {
		_panel = GetComponent<Image>();
		_defaultColor = new Color(_colorRed.r, 0, 0, 0);
		
	}

	public void OnBatHitPlayer() {
		Debug.Log("Redness");
		
		if (DOTween.IsTweening(_panel)) {
			
			DOTween.Kill(_panel);
			RednessScreen();
		}
		else {
			
			RednessScreen();
		}
		
	}

	void RednessScreen() {
		_panel.color = _colorRed;
		
		SmoothReturnStandartColor();
	}


	void SmoothReturnStandartColor() {
		_panel.DOColor(_defaultColor, _speedReturnDefaultColor);
	}
}

