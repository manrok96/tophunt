﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatFlyController : MonoBehaviour {

	float _currentAngle = 0;
	float _radius;
	Vector3 CircleCentr;
	public bool _isFly = false;

	[SerializeField] float _speed = 1;
	
	//public Vector3[] _wayPoints;
	Vector3 _endWayPoint;
	
	[SerializeField] float _minDistance = 10f;
	[SerializeField] float _maxDistance = 20f;

	[SerializeField] float _minHeight = 0.6f;
	[SerializeField] float _maxHeight = 2.1f;
	
	[SerializeField] float _minDelay = 1f;
	[SerializeField] float _maxDelay = 2f;
	
	CharacterController _characterController;
	
	Vector3 _lastObjectPosition;
	Vector3 _currentObjectPosition;
	Vector3 _moveDirection;
	
	float _batFlyHeight;

	//int _flyDirection;
	
	void Awake() {
		_characterController = FindObjectOfType<CharacterController>();
		
		gameObject.transform.position = GetRandomPositionAroundPlayer();
		gameObject.transform.rotation = Quaternion.LookRotation(_characterController.transform.position - transform.position);
		_batFlyHeight = Random.Range(_minHeight, _maxHeight);
		
		_lastObjectPosition = transform.position;
		GetNewWay();
		
	}

	void GetNewWay() {
		
		_endWayPoint = GetRandomPositionAroundPlayer();
		
		CircleCentr = GetCircleCenterPoint(transform.position,_characterController.transform.position, _endWayPoint);

		_radius = (CircleCentr - transform.position).magnitude;

		//_flyDirection = FlyDirection();
	}

	Vector3 GetRandomPositionAroundPlayer() {
		
		Vector2 random = Random.insideUnitCircle;
		Vector3 randomVector3 = new Vector3(random.x, 0f, random.y) * Random.Range(_minDistance, _maxDistance);
		
		while ((randomVector3 - _characterController.transform.position).magnitude < _minDistance ||
		       (randomVector3 - _characterController.transform.position).magnitude > _maxDistance ||
		       (randomVector3 - transform.position).magnitude < (_characterController.transform.position - transform.position).magnitude) {
		
			random = Random.insideUnitCircle;
			randomVector3 = new Vector3(random.x, 0, random.y) * Random.Range(_minDistance, _maxDistance);
		}
		
		randomVector3.y = _batFlyHeight;
		
		Debug.Log(Vector3.Angle(_characterController.transform.position - _endWayPoint, transform.forward));

		return randomVector3;
	}
	
	Vector3 GetCircleCenterPoint(Vector3 p1, Vector3 p2, Vector3 p3) {
		//вычисления центра окружности описанной по трем точкам
		float a = p2.x - p1.x;
		float b = p2.z - p1.z;
		float c = p3.x - p1.x;
		float d = p3.z - p1.z;
		
		float e = a * (p1.x + p2.x) + b * (p1.z + p2.z);
		float f = c * (p1.x + p3.x) + d * (p1.z + p3.z);
		
		float g = 2 * (a * (p3.z - p2.z) - b * (p3.x - p2.x));

		float x = (d * e - b * f) / g;
		float z = (a * f - c * e) / g;
		
		return new Vector3(x, 0, z);

	}
	
	
	void FixedUpdate() {
		

		if ((transform.position - _endWayPoint).magnitude <= 0.8f) {
		
			_isFly = false;
//			_currentAngle = 0;
			
			GetNewWay();
			BatFlyStart();
		}
		
		if (_isFly) {
			Fly();
		}
	}

	void Fly() {
		_currentAngle += Time.deltaTime;
		
		
		float x = Mathf.Cos(_currentAngle * _speed) /* _flyDirection */ * _radius;
		float z = Mathf.Sin(_currentAngle * _speed) /* _flyDirection */ * _radius;
		
		
		transform.position = new Vector3(x, _batFlyHeight, z) + CircleCentr;
		
		
		_currentObjectPosition = transform.position;
		
		_moveDirection = _currentObjectPosition - _lastObjectPosition;
		
		gameObject.transform.rotation = Quaternion.LookRotation(_moveDirection);
		_lastObjectPosition = _currentObjectPosition;

	}

//	int FlyDirection() {
////		if((transform.position - _characterController.transform.position).magnitude < ())
//		Vector3 dir = CircleCentr - transform.position;
//		Vector3 dir2 = CircleCentr - _characterController.transform.position;
//
//		float alpha = Vector3.Angle(dir, dir2);
//		Debug.Log(alpha);
//		
//		if (2*alpha < 360) {
//			return -1;
//		}
//		else {
//			return 1;
//		}
//	}

	IEnumerator BatFlyStartAfterRandomDelay() {
		yield return new WaitForSeconds(Random.Range(_minDelay,_maxDelay));
		BatFlyStart();
	}

	void BatFlyStart() {
		_isFly = true;
	}
}
