﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BatHitEvent : MonoBehaviour {

	public UnityEvent OnBatHit;
	
	void Start () {
		OnBatHit.AddListener(FindObjectOfType<RedScreen>().OnBatHitPlayer);
		
	}
	
	void OnTriggerEnter(Collider other) {
		OnBatHit.Invoke();
	}

	void OnDisable() {
		OnBatHit.RemoveListener(FindObjectOfType<RedScreen>().OnBatHitPlayer);
	}
}
