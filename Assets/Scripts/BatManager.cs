﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatManager : MonoBehaviour {

	[SerializeField] int _maxActiveMouse = 10;
	static int _currentActiveMouse = 0;
	
	
	static ObjectPooler _objectPooler;
	
	
	void Start() {
		_objectPooler = FindObjectOfType<ObjectPooler>();

		for (int activeMouse = 0; activeMouse < _maxActiveMouse; activeMouse++) {
			CreateMouse();
		}
	}

	
	static void CreateMouse() {
		
		_currentActiveMouse++;
		
		_objectPooler.GetObject();
	}

	public static void DestroyMouse(GameObject obj) {
		
		_currentActiveMouse--;
		
		_objectPooler.ReturnObject(obj);
	}

	void FixedUpdate() {
		if (_currentActiveMouse < _maxActiveMouse) {
			
			CreateMouse();
		}
	}
}
